import { ref, onMounted } from "vue";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Line } from "vue-chartjs";
import { FetchForecastData } from "../../../server/index";
import html2pdf from "html2pdf.js";

// Define variables for chart
ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

export default {
  components: {
    LineChart: Line,
  },
  data() {
    return {
      isButtonClicked: false,
    };
  },
  methods: {
    exportToPDF() {
      this.isButtonClicked = true;
      const element = document.getElementById("element-to-convert");

      // Check if element exists
      if (element) {
        html2pdf(element, {
          margin: 0.1,
          filename: "ForecastReport.pdf",
          image: { type: "jpeg", quality: 1 },
          html2canvas: {
            dpi: 300,
            legalRendering: true,
          },
          jsPDF: { unit: "in", format: "legal", orientation: "landscape" },
          scale: 1,
        });
      } else {
        console.error("Element not found.");
      }

      setTimeout(() => {
        window.location.reload();
      }, 500);
    },
  },
  setup() {
    const forecastX = ref([]);
    const forecastY = ref([]);
    const labels = ref([]);
    const datasets = ref([]);

    const mseErrorValue = ref("");
    const rsquaredErrorValue = ref("");

    onMounted(async () => {
      try {
        // Fetch forecasted data
        const forecastedData = await FetchForecastData("forecast", {
          endpoint: "",
          params: "",
        });

        // Clear previous data
        forecastX.value = [];
        forecastY.value = [];

        // Populate forecastX and forecastY arrays
        forecastedData.forEach((item) => {
          forecastX.value.push(item.months); // Assuming item.months is an array of months
          forecastY.value.push(item.target); // Assuming item.target is an array of quantity_sold
          mseErrorValue.value = item.mse;
          rsquaredErrorValue.value = item.r_squared.toFixed(2);
        });

        // Log forecastedY data array
        // console.log("ForecastedY:", forecastY.value);

        // Update chart data after fetching
        updateChartData();
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    });

    const updateChartData = () => {
      labels.value = forecastX.value.flat(); // Flatten the nested arrays if needed
      const quantityData = forecastY.value.flat(); // Flatten the nested arrays if needed
      const dataLength = quantityData.length;

      datasets.value = [
        {
          label: "Quantity",
          data: quantityData,
          fill: false,
          borderColor: "#FFB200",
          pointBackgroundColor: Array(dataLength).fill("#FFB200"), // Set all points to #FFB200 initially
          pointBorderColor: Array(dataLength).fill("#FFB200"),
          pointRadius: 5,
          tension: 0.1,
        },
      ];

      // Set different color for the last three data points
      if (dataLength > 0) {
        const lastThreeDataPointsIndices = [
          dataLength - 3,
          dataLength - 2,
          dataLength - 1,
        ];

        // Set different color for the last three data points
        lastThreeDataPointsIndices.forEach((index) => {
          datasets.value[0].pointBackgroundColor[index] = "#4339F2";
          datasets.value[0].pointBorderColor[index] = "#4339F2";
        });
      }
    };

    //Options for Chart
    const options = ref({
      responsive: true,
      plugins: {
        legend: {
          display: false,
          labels: {
            font: {
              family: "Poppins",
            },
          },
        },
      },
    });

    ChartJS.defaults.font.family = "Poppins";

    const roundValue = (value) => {
      // Round off the value to 2 decimal places
      return Number(value).toFixed(2);
    };

    return {
      labels,
      datasets,
      options,
      mseErrorValue,
      rsquaredErrorValue,
      forecastX,
      forecastY,
      roundValue,
    };
  },
};
