import { ref, onMounted, onBeforeUnmount } from "vue";
import { useRouter, useRoute } from "vue-router";
import { useQuasar, LocalStorage } from "quasar";
// import { Search, currentRows } from "../../../server/index";
import Filters from "../../../components/FiltersForSales.vue";
import Pagination from "../../../components/PaginationComp.vue";
import { Search, currentRows } from "../../../composables/SearchForSales";

// fetch sales data from db
import {
  FetchSales,
  FetchSalesData,
  AddSalesData,
  EditSalesData,
  DeleteSalesData,
  AddForecastData,
  ResetForecastData,
  FetchForecastData,
  ForecastSales,
} from "../../../server/index";

export default {
  components: {
    Filters,
    Pagination,
  },
  props: {
    dynamicOptionsForUnitName: {
      type: Array,
      default: () => [
        {
          value: "Animal Production",
          label: "Animal Production",
        },
        {
          value: "Coffee Processing Center",
          label: "Coffee Processing Center",
        },
        {
          value: "International Convention Center",
          label: "International Convention Center",
        },
        {
          value: "International House II",
          label: "International House II",
        },
        {
          value: "Marketing Center",
          label: "Marketing Center",
        },
        {
          value: "Printing Services Unit",
          label: "Printing Services Unit",
        },
        {
          value: "Student Dormitory",
          label: "Student Dormitory",
        },
        {
          value: "Swimming Pool",
          label: "Swimming Pool",
        },
        {
          value: "Transport Service",
          label: "Transport Service",
        },
        {
          value: "University Entrep. Center",
          label: "University Entrep. Center",
        },
        {
          value: "Rentals and Concessions",
          label: "Rentals and Concessions",
        },
        {
          value: "Rolle Hall",
          label: "Rolle Hall",
        },
        {
          value: "Water Refilling Station",
          label: "Water Refilling Station",
        },
      ],
    },
  },
  methods: {
    formatDate(value) {
      // Parse the input value as MM/YYYY
      const [month, year] = value.split("-");
      // Set the selectedDate to the first day of the month in YYYY-MM-DD format
      this.selectedDate = `${year}-${month.padStart(2, "0")}-01`;
    },
  },
  setup(props) {
    onMounted(() => {
      // Fetch data for the table
      FetchSalesData("sales", {
        endpoint: "",
        params: "",
      }).then((response) => {
        rows.value = response;
        Search.value = rows.value.slice(0, pagination.value.rowsPerPage);
      });
    });

    const router = useRouter(); // Router
    const $q = useQuasar(); // q

    let rows = ref([]); // Rows

    // Column Headers
    const columns = [
      {
        name: "id",
        field: "id",
        label: "Sales ID",
        align: "center",
        sortable: true,
        headerStyle: "text-align: center;",
        style: "font-weight: normal; text-align: center; min-width: 64px;",
      },
      {
        name: "unitName",
        field: "unit_name",
        label: "Unit Name",
        align: "left",
        sortable: true,
        style: "font-weight: normal;",
      },
      {
        name: "totalSales",
        field: "total_sales",
        label: "Total Sales",
        align: "center",
        sortable: false,
        style: "font-weight: normal;",
      },
      {
        name: "date",
        field: "date",
        label: "Date",
        align: "center",
        sortable: true,
        style: "font-weight: normal;",
      },
      {
        name: "actions",
        label: "Action",
        align: "center",
        field: "actions",
        headerStyle: "text-align: left;",
        style: "text-align: center; width: 80px !important;",
      },
    ];

    // Pagination
    const pagination = ref({
      sortBy: "desc",
      descending: false,
      page: 1,
      rowsPerPage: 5,
    });

    /*  This function updates the row data in the column whenever called.
        It updates the startIndex and endIndex and then proceed with updating the currentRows or Search.
        currentRows/Search are the rows shown in the table. */

    const updatePagination = () => {
      const startIndex =
        (pagination.value.page - 1) * pagination.value.rowsPerPage;
      const endIndex = startIndex + pagination.value.rowsPerPage - 1;
      Search.value = rows.value.slice(startIndex, endIndex + 1);
    };

    // Function to update rowsPerPage according to the length of rows
    const showAllRows = () => {
      pagination.value.rowsPerPage = rows.value.length;
      updatePagination(); // Call the function to update the pagination
    };

    // Form
    const form = ref({
      id: null,
      unitName: "",
      totalSales: null,
      date: "",
    });

    // Function to check if form is valid
    const isFormValid = () => {
      return (
        form.value.unitName !== "" &&
        form.value.totalSales !== "" &&
        form.value.date !== ""
      );
    };

    // Function to clear all inputs when cancel is clicked
    const fetchAll = () => {
      form.value.unitName = "";
      form.value.totalSales = null;
      form.value.date = null;
    };

    // Function to add data
    const addSalesData = () => {
      /* Counts the accounts first to ensure new id
      in accordance with INT format. */
      FetchSales().then((response) => {
        let accounts = response;
        let highestId = 0;

        // Find the highest existing ID
        accounts.forEach((account) => {
          if (account.id > highestId) {
            highestId = parseInt(account.id); // Parse ID as integer
          }
        });

        // Increment the ID for the new account
        const newId = highestId + 1;

        // Equates form values
        const data = {
          // id: `${newId}`,
          id: newId,
          unit_name: form.value.unitName,
          total_sales: Number(form.value.totalSales),
          date: form.value.date,
        };

        // Add the new data to the server
        AddSalesData(data).then((response) => {
          // data.id = newId; // Ensure newId is a number
          // Number(data.total_sales);

          let status = Boolean(response.data !== "undefined");
          $q.notify({
            // Post notification on lower right side on the organizational management page
            position: $q.screen.width < 767 ? "top" : "bottom-right",
            classes: `${
              status ? "my-success-notif" : "my-error-notif"
            } q-px-lg q-pt-none q-pb-none q-mr-lg q-mb-md`,
            html: true,
            message: status
              ? `<div class="text-bold">Added Succesfully!</div> A new record has been added.`
              : `<div class="text-bold">Failed to add the new record!</div>`,
          });
        });

        // Update table data after adding
        FetchSalesData("sales", {
          endpoint: "",
          params: "",
        }).then((response) => {
          rows.value = response;
          Search.value = rows.value.slice(0, pagination.value.rowsPerPage);
        });
      });
    };

    /* Method to handle when the row is clicked.
    Prepopulates Edit Input Fields in accordance with the row id */
    const pushAccount = (row) => {
      form.value.id = row._id;
      form.value.unitName = row.unit_name;
      form.value.totalSales = row.total_sales;
      form.value.date = row.date;
    };

    // Function to Edit Account details
    const editRecord = () => {
      // Equates form values
      const dataToEdit = {
        _id: form.value.id,
        unit_name: form.value.unitName,
        total_sales: Number(form.value.totalSales),
        date: form.value.date,
      };

      // Actually edit details
      EditSalesData(dataToEdit)
        .then((response) => {
          let status = Boolean(response.data !== "undefined");
          $q.notify({
            // Post notification on lower right side on the organizational management page
            position: $q.screen.width < 767 ? "top" : "bottom-right",
            classes: `${
              status ? "my-success-notif" : "my-error-notif"
            } q-px-lg q-pt-none q-pb-none q-mr-lg q-mb-md`,
            html: true,
            message: status
              ? `<div class="text-bold">Record was changed succesfully!</div> Record has been updated.`
              : `<div class="text-bold">Failed to update the record!</div>`,
          });
        })
        .finally(() => {
          // Update table data after editing
          FetchSalesData("sales", {
            endpoint: "",
            params: "",
          }).then((response) => {
            rows.value = response;
            Search.value = rows.value.slice(0, pagination.value.rowsPerPage);
            fetchAll();
          });
        });
    };

    // Reference variable for the row to delete
    const rowToDelete = ref({
      id: null,
      unitName: "",
      totalSales: null,
      date: "",
    });

    // Function to remember the row values when delete button is clicked
    const rememberRowToDelete = (row) => {
      // Copy the row values to the reactive variable
      rowToDelete.value.id = row._id;
      rowToDelete.value.subId = row.id;
      rowToDelete.value.unitName = row.unit_name;
      rowToDelete.value.totalSales = row.total_sales;
      rowToDelete.value.date = row.date;
    };

    // Function to delete the account
    const deleteRecord = () => {
      // Perform delete operation using the rowToDelete
      DeleteSalesData({ _id: rowToDelete.value.id });

      // Post notification on lower right side of the page
      $q.notify({
        position: $q.screen.width < 767 ? "top" : "bottom-right",
        classes: `my-success-notif q-px-lg q-pt-none q-pb-none q-mr-lg q-mb-md`,
        html: true,
        message: `<div class="text-bold">Record deleted successfully!</div> Record No. ${rowToDelete.value.subId} has been deleted.`,
      });

      // Update table data after deleting
      FetchSalesData("sales", {
        endpoint: "",
        params: "",
      }).then((response) => {
        rows.value = response;
        Search.value = rows.value.slice(0, pagination.value.rowsPerPage);
      });
    };

    // Dynamic options for unit names/category filter
    let optionsForUnitName = ref(props.dynamicOptionsForUnitName);

    // // for loading components
    // let timer;

    // onBeforeUnmount(() => {
    //   if (timer !== void 0) {
    //     clearTimeout(timer);
    //     $q.loading.hide();
    //   }
    // });

    const loading = ref(false);

    // Function to add forecast data to db
    const addForecastData = () => {
      // Data to forecast reference
      const dataToForecast = ref();

      // Ensures that forecast collection is empty first
      FetchForecastData("forecasts", {
        endpoint: "",
        params: "",
      }).then((response) => {
        dataToForecast.value = response;
        dataToForecast.value.forEach((record) => {
          ResetForecastData({ _id: record._id }).then(() => {});
        });
      });

      // Extract dates and total_sales from rows.value
      Search.value.map((row) => row.date);
      Search.value.map((row) => row.total_sales);

      // Create a dictionary to store total sales for each unique date
      const salesByDate = {};
      Search.value.forEach((row) => {
        const date = row.date;
        const totalSales = row.total_sales;
        if (salesByDate.hasOwnProperty(date)) {
          salesByDate[date] += totalSales;
        } else {
          salesByDate[date] = totalSales;
        }
      });

      // Initialize arrays for months and sales
      const months = [];
      const salesAggregated = [];

      // Iterate over unique dates and add corresponding total sales to arrays
      for (const date in salesByDate) {
        if (salesByDate.hasOwnProperty(date)) {
          months.push(date);
          salesAggregated.push(salesByDate[date]);
        }
      }

      // Generate an array of numbers from 1 to the length of the months array
      const monthsId = Array.from(
        { length: months.length },
        (_, index) => index + 1
      );

      // Equates form values
      const data = {
        months: months,
        months_id: monthsId, // Assign the generated months_id array
        target: salesAggregated,
      };

      // Add the new data to the server
      AddForecastData(data).then((response) => {
        ForecastSales();
      });

      // $q.loading.show({
      //   message: "Forecasting data. Please wait for a moment...",
      // });

      // /* Hides loading component after 10s
      // and pushes to forecast page */
      // timer = setTimeout(() => {
      //   $q.loading.hide();
      //   timer = void 0;
      //   router.push({ path: "/forecast-sales" });
      // }, 8000);

      loading.value = true;

      /* Hides loading component after 8s
      and pushes to forecast page */
      setTimeout(() => {
        // $q.loading.hide();
        loading.value = false;
        router.push({ path: "/forecast-sales" });
      }, 8000);
    };

    const userType = LocalStorage.getItem("userSession");
    const checkIfAdmin = Boolean(userType.isAdmin === true);

    return {
      Search,
      currentRows,
      router,
      rows,
      columns,
      pagination,
      updatePagination,
      showAllRows,
      form,
      isFormValid,
      fetchAll,
      addSalesData,
      pushAccount,
      editRecord,
      rememberRowToDelete,
      deleteRecord,
      optionsForUnitName,
      addForecastData,
      loading,
      checkIfAdmin,

      // References
      addNewRecordDialog: ref(false),
      editRecordDialog: ref(false),
      deleteRecordDialog: ref(false),
    };
  },
};
