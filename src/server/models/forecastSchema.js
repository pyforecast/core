const mongoose = require("mongoose");

const forecastSchema = new mongoose.Schema({
  id: Number,
  months: Array,
  months_id: Array,
  target: Array,
  mse: Number,
  r_squared: Number,
});

module.exports = mongoose.model("Forecast", forecastSchema);
